export const fhirAPIBaseURL = 'https://nzlfhirsrvt7ss7ccrrakte.azurewebsites.net/';
export const allergyIntoleranceExtensionBaseURI = 'https://fhir-server.org/fhir/StructureDefinition/adverseevent-';
export const patientExtensionBaseURI = 'https://fhir-server.org/fhir/StructureDefinition/patient-';
