/// <reference path="../../../node_modules/@types/fhir/index.d.ts" />
import f = fhir;

import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {fhirAPIBaseURL} from "../config";

@Injectable()
export class FHIRService {

  // URL to a valid FHIR server (you may need to change this if you're trying this out on with a different FHIR server)
  FHIR_SERVER_BASE_URL: string = fhirAPIBaseURL;

  constructor(private http: HttpClient) {
  }

  /**
   * GET a resource from the FHIR server
   * resourcePath is of the form ResourceName/resourceID e.g. Patient/12345
   */
  getResource<T>(resourcePath: string): Observable<T> {
    return this.http.get(this.FHIR_SERVER_BASE_URL + resourcePath);
  }

  /**
   * GET the root clinical Encounter
   */
  getEncounter(encounterID: string): Observable<f.Encounter> {
    return this.getResource(`Encounter/${encounterID}`);
  }

  /**
   * GET the prescribed medications for the given patient
   */
  getMedicationStatements(patientID: string): Observable<any> {
    return this.getResource(`MedicationStatement?_subject=${patientID}`);
  }

  /**
   * POST the AllergyIntolerance resource
   */
  postAllergyIntolerance(allergyIntolerance: any): Observable<any> {
    console.log("About to POST this AllergyIntolerance resource", allergyIntolerance);
    return this.http.post(this.FHIR_SERVER_BASE_URL + "AllergyIntolerance", allergyIntolerance, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    });
  }
}

