export class AllergyIntoleranceExtension {
  manifestation?: string;
  onset?: string;
  outcome?: string;
  severity?: string;
  rechallenged?: boolean;
  rechallengeResult?: string;
  fatal?: boolean;
  fatalityDate?: string;
  isFollowingImmunisation?: boolean;
}

