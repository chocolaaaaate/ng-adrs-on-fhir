import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, NgModel} from '@angular/forms';

import {AppComponent} from './app.component';
import {CarmFormComponent} from './carm-form/carm-form.component';
import {FHIRService} from './services/fhir.service';
import {HttpClientModule} from '@angular/common/http';
import {routing} from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    CarmFormComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, routing
  ],
  providers: [FHIRService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
