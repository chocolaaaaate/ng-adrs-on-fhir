/// <reference path="../../../node_modules/@types/fhir/index.d.ts" />

import {Component, OnDestroy, OnInit} from '@angular/core';
import {FHIRService} from '../services/fhir.service';
import {ActivatedRoute} from '@angular/router';
import {AllergyIntoleranceExtension} from "../model/Reaction";
import {patientExtensionBaseURI, allergyIntoleranceExtensionBaseURI} from "../config";
import f = fhir;


@Component({
  selector: 'carm-form',
  templateUrl: './carm-form.component.html',
  styleUrls: ['./carm-form.component.css']
})
export class CarmFormComponent implements OnInit, OnDestroy {

  routeSubscription: any;
  dataReady: boolean;
  errorMessage: string;
  submissionSuccessful: boolean = false;

  /**
   * FHIR Resources
   */
  encounter: f.Encounter;
  patient: f.Patient;
  practitioner: f.Practitioner;
  medicationStatements: f.MedicationStatement[];

  /**
   * FHIR resource extensions
   */
  allergyIntoleranceExtension: AllergyIntoleranceExtension;
  patientExtension: { weight: number, ethnicity: string };
  practitionerPhone: string;
  practitionerEmail: string;
  practitionerProfession: string;

  allergyIntoleranceExtensionBaseURI: string = allergyIntoleranceExtensionBaseURI;
  patientExtensionBaseURI: string = patientExtensionBaseURI;

  constructor(private route: ActivatedRoute, private fhirService: FHIRService) {
    this.dataReady = false;
    this.errorMessage = undefined;
    this.allergyIntoleranceExtension = new AllergyIntoleranceExtension();
    this.patientExtension = {
      weight: undefined,
      ethnicity: undefined
    }
    this.practitionerProfession = 'Physician';
  }


  /**
   * Pre-populate the form on initialization by retreiving the core clinical Encounter resource first,
   * then the doctor and patient involved in this clinical encounter, and finally the medications for the patient.
   */
  ngOnInit() {
    // URL parameter is the ID for the clinical Encounter
    this.routeSubscription = this.route.params.subscribe(params => {
        this.fhirService.getEncounter(params['encounter']).subscribe(
          (encounter) => {
            this.encounter = encounter;
            console.log("Encounter:", this.encounter);

            // Get the doctor involved in this clinical encounter
            this.fhirService.getResource<f.Practitioner>(encounter.participant[0].individual.reference).subscribe(
              (practitioner: f.Practitioner) => {
                this.practitioner = practitioner;
                console.log("Practitioner:", this.practitioner);

                this.practitionerEmail = this.practitioner.telecom.filter((t) => {
                  return t.system === 'email'
                })[0].value;
                this.practitionerPhone = this.practitioner.telecom.filter((t) => {
                  return t.system === 'phone'
                })[0].value;

                // Get the patient in this clinical encounter
                this.fhirService.getResource<f.Patient>(encounter.subject.reference).subscribe(
                  (patient: f.Patient) => {
                    this.patient = patient;
                    console.log("Patient:", this.patient);

                    // Get the patient's medications
                    this.fhirService.getMedicationStatements(this.patient.id).subscribe(
                      (medStatements) => {
                        this.medicationStatements = [];

                        if (medStatements.resourceType === 'Bundle') {
                          // if bundle - map and add
                          const tempMeds = medStatements.entry.map((e) => {
                            return e.resource;
                          });
                          this.medicationStatements.push(...tempMeds);
                        }
                        else if (medStatements.resourceType === 'MedicationStatement') {
                          // if single - push alone
                          const tempMed = medStatements;
                          this.medicationStatements.push(tempMed);
                        }
                        console.log("MedicationStatements:", this.medicationStatements);
                        this.dataReady = true;
                      },
                      (err) => {
                        this.errorMessage = err;
                      }
                    );
                  },
                  (err) => {
                    this.errorMessage = err;
                  }
                );
              },
              (err) => {
                this.errorMessage = err;
              }
            );
          },
          (err) => {
            this.errorMessage = err;
          },
          () => {

          }
        );
      },
      (err) => {
        this.errorMessage = err;
      }
    );
  }


  /*
  * Collect pre-populated and user-entered input to collate a FHIR resource (AllergyIntolerance) and POST this resource.
  */
  submitForm() {
    // extend patient resource before posting
    this.patient.extension = [];
    if (this.patientExtension.ethnicity) {
      this.patient.extension.push({
        url: this.patientExtensionBaseURI + 'ethnicity',
        valueString: this.patientExtension.ethnicity
      });
    }
    if (this.patientExtension.weight) {
      this.patient.extension.push({
        url: this.patientExtensionBaseURI + 'weight',
        valueInteger: this.patientExtension.weight
      });
    }

    // construct the AllergyIntolerance resource to be posted to the FHIR endpoint
    const allergyIntolerance: f.AllergyIntolerance = {
      resourceType: 'AllergyIntolerance',
      patient: this.patient,
      recorder: this.practitioner,
      code: {
        coding: this.medicationStatements[0].medicationCodeableConcept.coding
      },
      reaction: [
        {
          manifestation: [
            this.allergyIntoleranceExtension.manifestation
          ],
          onset: this.allergyIntoleranceExtension.onset
        }
      ],
      category: ["medication"],
      clinicalStatus: "active",
      verificationStatus: 'unconfirmed',
      extension: [
        {
          url: this.allergyIntoleranceExtensionBaseURI + 'severity',
          valueString: this.allergyIntoleranceExtension.severity
        },
        {
          url: this.allergyIntoleranceExtensionBaseURI + 'rechallenged',
          valueBoolean: this.allergyIntoleranceExtension.rechallenged
        },
        {
          url: this.allergyIntoleranceExtensionBaseURI + 'fatalBoolean',
          valueBoolean: this.allergyIntoleranceExtension.fatal
        },
        {
          url: this.allergyIntoleranceExtensionBaseURI + 'outcome',
          valueString: this.allergyIntoleranceExtension.outcome
        },
        {
          url: this.allergyIntoleranceExtensionBaseURI + 'isFollowingImmunisation',
          valueBoolean: this.allergyIntoleranceExtension.isFollowingImmunisation
        }
      ]
    };

    if (this.allergyIntoleranceExtension.rechallengeResult) {
      allergyIntolerance.extension.push({
        url: this.allergyIntoleranceExtensionBaseURI + 'rechallengeResult',
        valueString: this.allergyIntoleranceExtension.rechallengeResult
      });
    }
    if (this.allergyIntoleranceExtension.fatalityDate) {
      allergyIntolerance.extension.push({
        url: this.allergyIntoleranceExtensionBaseURI + 'fatalityDate',
        valueString: this.allergyIntoleranceExtension.fatalityDate
      });
    }

    // POST the AllergyIntolerance resource to the end point.
    this.fhirService.postAllergyIntolerance(allergyIntolerance).subscribe(
      (next) => {
        this.submissionSuccessful = true;
      },
      (err) => {
        this.errorMessage = err;
      },
      () => {
        console.log("Submitted successfully")
      }
    );

  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

}

