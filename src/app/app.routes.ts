import {RouterModule, Routes} from '@angular/router';
import {CarmFormComponent} from "./carm-form/carm-form.component";

export const routes: Routes = [
  {path: 'carm/:encounter', component: CarmFormComponent}
];

export const routing = RouterModule.forRoot(routes);
