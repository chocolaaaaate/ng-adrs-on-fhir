# Angular FHIR Demo 

This project is part of the NZ FHIR Challenge held in Auckland, in March 2018. 

This is a demo Angular 5 project that reads FHIR resources and prepopulates a version of the [CARM form](https://nzphvc.otago.ac.nz/report/) - a nation-wide form for reporting medication allergy/intolerance. Pre-populating this form will save a lot of time and remove a known barrier in reporting adverse drug reaction reports to CARM (a national authority).  

The form that starts off with the root FHIR resource (representing a clinical encounter in this case), and it traverses references in that root resource (i.e. the HATEOS bit of REST API spec) to find related resources involved (such as the patient and doctor resources, and then the active medications resource referenced by the patient in turn). 

On submission, the form packages up pre-populated data + new user inputs into another FHIR resource - [AllergyIntolerance](https://www.hl7.org/fhir/allergyintolerance.html) in our case). This resource can then be POSTed to a FHIR-compliant endpoint. So the form is FHIR in, FHIR out. 

*Note: this was for a hackathon with limited time, so it won't be the prettiest code!*

To run this application do `npm start` and then go to: `http://localhost:4200/carm/<encounterID>` where `encounterID` is the resource ID for a FHIR [Encounter](https://www.hl7.org/fhir/encounter.html) resource. You'll also need to update the endpoint in `config.ts` to point to a valid FHIR endpoint. The server provided during FHIR Challenge hackathon may not be available at the time you're viewing this repo.

Dr. Sumedh Kanade (MBChB, BE), March 2018
